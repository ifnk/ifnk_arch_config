# -*- coding: utf-8 -*-

import re
from xkeysnail.transform import *

#先将大小写转换键替换成点击F13，长按为Super键
define_multipurpose_modmap(
	{
		# ;
		Key.SEMICOLON: [Key.F14, Key.RIGHT_ALT],
		# caps
		Key.CAPSLOCK: [Key.F13,Key.RIGHT_META ],
		# tab
		Key.TAB: [Key.TAB, Key.LEFT_CTRL],
		# shift
		Key.SLASH: [Key.SLASH, Key.LEFT_SHIFT],
		# Key.SPACE: [Key.SPACE, Key.RIGHT_CTRL],
		# ' "
		Key.APOSTROPHE: [Key.ENTER, Key.RIGHT_CTRL],
		# [
		Key.LEFT_BRACE: [Key.BACKSPACE, Key.LEFT_CTRL],
	}
)

#全局RCtrl 映射
define_keymap(None, {
	#大写 的映射
	K("f13"): K("ESC"),
	K("LShift-f13"): K("LShift-ESC"),
	K("RShift-f13"): K("RShift-ESC"),
	K("RSuper-l"): K("right"),
	K("RSuper-k"): K("up"),
	K("RSuper-h"): K("left"),
	K("RSuper-j"): K("down"),
	K("RSuper-n"): K("LSuper-h"),
	K("RSuper-m"): K("LSuper-l"),
	K("RSuper-Space"): K("LSuper-l"),
	K("RSuper-semicolon"): K("page_down"),
	K("RSuper-p"): K("page_up"),
	K("RSuper-u"): K("home"),
	K("RSuper-d"): K("delete"),
	K("RSuper-i"): K("LSuper-H"),
	K("RSuper-o"): K("LSuper-L"),
	K("RSuper-LM-H"): K("LM-LEFT"),
	K("RSuper-LM-L"): K("LM-RIGHT"),
	K("RSuper-LC-H"): K("LC-LEFT"),
	K("RSuper-LC-L"): K("LC-RIGHT"),

	#分号 的映射
	K("F14"): K("SEMICOLON"),
	K("LShift-F14"): K("LShift-SEMICOLON"),
	K("C-F14"): K("LC-SEMICOLON"),
	K("Super-F14"): K("Win-SEMICOLON"),

	K("RM-A"): K("LShift-KEY_1"),
	K("RM-B"): K("LShift-KEY_5"),
	K("RM-C"): K("LShift-APOSTROPHE"),
	K("RM-D"): K("EQUAL"),
	K("RM-E"): K("LShift-KEY_9"),
	K("RM-F"): K("BACKSLASH"),
	K("RM-G"): K("LShift-KEY_8"),
	K("RM-H"): K("LShift-KEY_7"),
	K("RM-I"): K("LShift-GRAVE"),
	K("RM-J"): K("LShift-KEY_3"),
	K("RM-M"): K("SLASH"),
	K("RM-N"): K("LShift-KEY_6"),
	K("RM-Q"): K("LShift-MINUS"),
	K("RM-R"): K("LShift-KEY_0"),
	K("RM-S"): K("LShift-KEY_4"),
	K("RM-T"): K("LShift-LEFT_BRACE"),
	K("RM-U"): K("LShift-BACKSLASH"),
	K("RM-V"): K("MINUS"),
	K("RM-W"): K("LShift-SLASH"),
	K("RM-X"): K("LShift-EQUAL"),
	K("RM-Y"): K("LShift-RIGHT_BRACE"),
	K("RM-Z"): K("APOSTROPHE"),
	K("RM-SPACE"): K("LShift-SEMICOLON"),
	K("RM-KEY_1"): K("LEFT_BRACE"),
	K("RM-KEY_2"): K("RIGHT_BRACE"),

	#RCtrl 的映射
	K("RC-A"): K("LWin-KEY_1"),
	K("RC-S"): K("LWin-KEY_2"),
	K("RC-W"): K("LWin-KEY_4"),
	# K("RC-E"): K("RWin-KEY_5"),
	K("RC-Q"): K("LWin-KEY_3"),
	K("RC-E"): K("LWin-KEY_5"),
	K("RC-Z"): K("LWin-KEY_7"),
	K("RC-C"): K("LWin-KEY_8"),
	K("RC-V"): K("LWin-KEY_9"),
	K("RC-X"): K("LC-F4"),

	K("RC-B"): K("Win-W"),
	# K("RC-TAB"): K("LWin-ENTER"),

	K("RC-E"): K("LWin-E"),
	K("RC-H"): K("LSuper-H"),
	K("RC-J"): K("LSuper-J"),
	K("RC-K"): K("LSuper-K"),
	K("RC-L"): K("LSuper-L"),
	# 寻找存在的窗口
	# K("RC-F"): K("LWin-TAB"),

	K("RC-N"): K("LSuper-h"),
	K("RC-M"): K("LSuper-l"),
})
# jetbrains ide 映射
define_keymap(re.compile("jetbrains-rider|jetbrains-studio|jetbrains-webstorm|jetbrains-clion"), {
	# 显示方法 
	K("RC-Space"): K("C-F12"),
	# 寻找文件 
	K("RC-F"): K("LC-LShift-N"),
	# 将光标(焦点)弄到代码编辑器上 
	K("RC-D"): K("LM-HOME"),
	# 停止运行 stop
	# K("RC-O"): K("LC-F2"),
	# 运行
	# K("RC-I"): K("LShift-F9"),
	# 下一个tab
	K("RC-O"): K("LM-RIGHT"),
	# 上一个tab
	K("RC-I"): K("LM-LEFT"),
	#  切换左边的 tab 或者 右边的 tab
	# 重构
	K("RC-T"): K("LShift-LM-LC-T"),
	# 重构
	K("RC-R"): K("LShift-F6"),
	# 生成成员之类的
	K("RC-G"): K("LM-INSERT"),
	# 启动 debug
	K("RC-KEY_1"): K("LShift-F9"),
	# 停止调试 stop
	K("RC-KEY_2"): K("LC-F2"),
	# 下一步调试 next
	K("RC-KEY_3"): K("F9"),
	# 关掉所有没用的窗口 
	K("RC-KEY_4"): K("LShift-LC-F12"),
	# 关掉所有没用的窗口 
	K("RC-GRAVE"): K("LShift-LC-F12"),
}, "Firefox and Chrome")
# chrome 映射
define_keymap(re.compile("Google-chrome"), {
	K("RC-O"): K("LC-TAB"),
	K("RC-I"): K("LC-Shift-TAB"),
	K("LM-f"): K("LC-right"),
	K("LM-b"): K("LC-left"),
	K("LC-W"): K("LC-backspace"),
	K("LC-Q"): K("LC-F4"),
	K("LC-u"): [ K("LC-a"),K("Backspace")],
	# 切换焦点 到网页 
	K("RC-G"): [ K("LM-D"),K("F6"),K("F6") ],
	# 切换焦点 开发者模式 devTools  
	K("RC-F"): [ K("LM-D"),K("F6"),K("F6"),K("F6")],
}, " Chrome")
# wps 映射
define_keymap(re.compile("Wps|Et"), {
	K("RC-O"): K("LC-TAB"),
	K("RC-I"): K("LC-Shift-TAB"),
}, "Firefox and Chrome")

