"  这里是vim-plug 的一些插件 
"
"
"
"安装插件 
call plug#begin('~/.vim/plugged')
" fzf 插件
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
" Plug 'valloric/MatchTagAlways'
Plug 'jiangmiao/auto-pairs'



" vim 注释插件 
Plug 'https://github.com/tpope/vim-commentary.git'
" vim surround  插件 
Plug 'https://github.com/tpope/vim-surround.git'
" vim 高亮  插件 
Plug 'https://github.com/machakann/vim-highlightedyank.git'
" vim 交换  插件 
Plug 'https://github.com/tommcdo/vim-exchange.git'
" vim gr 替换   插件 
Plug 'https://github.com/vim-scripts/ReplaceWithRegister.git'
" vim 快速选择函数参数   插件 
Plug 'https://github.com/vim-scripts/argtextobj.vim.git'
" vim log文件高亮插件 
Plug 'mtdl9/vim-log-highlighting'




" vim 美化插件  
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" vim 主题插件  
Plug 'flazz/vim-colorschemes'


" vim coc 补全插件   
" Plug 'neoclide/coc.nvim', {'branch': 'release'}
"
" vim ranger插件  
Plug 'kevinhwang91/rnvimr'
" vim c# 插件  
Plug 'OmniSharp/omnisharp-vim'
Plug 'dense-analysis/ale'
" 语法突出
Plug 'sheerun/vim-polyglot'



" vim plug 插件结尾 
call plug#end()

