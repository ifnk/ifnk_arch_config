" 这是nvim 的基础设置


" 语法高亮 
syntax on 
" 显示行号
set nu
" 显示相对行号
set relativenumber
set showmode
"搜索高亮显示
set hls
" 启动的时候取消高亮单词 
exec "nohlsearch"
set incsearch

set tabstop=4
set softtabstop=4
set shiftwidth=4
" 取消自动换行 把textwidth调大
set textwidth=1000
" 取消自动换行
set nowrap
set showcmd	
set wildmenu		

" 搜索时忽略大小写
set ignorecase
set smartcase


