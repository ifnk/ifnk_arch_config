" 这个是 vim 的 按键绑定设置(快捷键)
"
"
"跳回上次的位置
" nnoremap q `.
"重载vimrc 配置 
nnoremap R :source $MYVIMRC<CR>

nnoremap <C-H> <C-W>h
nnoremap <C-L> <C-W>l
nnoremap <C-K> <C-W>k
nnoremap <C-J> <C-W>j

" 打开ranger 文件查找器
nnoremap <M-1> :RnvimrToggle<CR>

vnoremap <C-C> "+y
vnoremap <C-Insert> "+y
vnoremap <C-V> "+P
imap <C-V> <C-R>+
map <S-Insert> "+P
cmap <C-V> <C-R>+
cmap <S-Insert> <C-R>+

"替换当前选中的文本
vnoremap <Leader>r "ry:%s/<C-R>r/


"visual 模式 下 按* 号 搜索 光标选中的内容(对已选中的内容进行查找)
vmap * "ry/<C-R>r<CR>N

" 全部选中
map <leader>sa ggVG
nnoremap <c-s> :w<CR>

"行复制
nnoremap gp gP

"替换插件 ReplaceWithRegister
vnoremap n ^
vnoremap q g_
"
onoremap n ^
onoremap q g_
""内部
onoremap w iw
onoremap , i(
onoremap ( i(
onoremap ie i(
onoremap r i[
onoremap m i{
onoremap . i<
onoremap z i'
onoremap c i"
onoremap \ it
""外部
onoremap aw aw
onoremap a, a(
onoremap ae a(
onoremap ar a[
onoremap am a{
onoremap a. a<
onoremap az a'
onoremap ac a"
onoremap a\ at

"内部
vnoremap w iw
vnoremap ( i(
vnoremap ie i(
vnoremap ir i[
vnoremap m i{
vnoremap . i<
vnoremap z i'
vnoremap c i"
"外部
vnoremap aw aw
vnoremap a, a(
vnoremap ae a(
vnoremap ar a[
vnoremap am a{
vnoremap a. a<
vnoremap az a'
vnoremap ac a"


